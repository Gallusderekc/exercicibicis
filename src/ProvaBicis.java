import cat.itb.exercicibicis.Bici;

import java.time.LocalTime;

public class ProvaBicis {
    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        Bici b1 = new Bici("Mosne", inici, 200, 30);
        Bici b2 = new Bici("Carla", inici, 200, 30);
        Bici b3 = new Bici("Farn", inici, 200, 30);
        b1.start();
        b2.start();
        b3.start();
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);
    }
}
