package cat.itb.exercicibicis;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalTime;
import java.util.Scanner;

public class Bici extends Thread {
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;

    public Bici(String nom, LocalTime inici, int distancia, long temps) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
        this.temps = temps;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalTime getInici() {
        return inici;
    }

    public void setInici(LocalTime inici) {
        this.inici = inici;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public long getTemps() {
        return temps;
    }

    public void setTemps(long temps) {
        this.temps = temps;
    }

    @Override
    public String toString() {
        return "Bici{" +
                "nom='" + nom + '\'' +
                ", inici=" + inici +
                ", distancia=" + distancia +
                ", temps=" + temps +
                '}';
    }


    @Override
    public void run(){
        System.out.println("\n "+nom +" "+distancia +" "+ inici +
                " estan competint "+ getNom()+" "+ getDistancia()+ " "+ getInici() +" ");
        for(int i=0;i<10;i++){
            System.out.println(nom +" "+i);
        }
    }
}
